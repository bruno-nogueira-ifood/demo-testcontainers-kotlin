package nom.brunokarpo.demotestcontainerskotlin

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class DemoTestcontainersKotlinApplication

fun main(args: Array<String>) {
	runApplication<DemoTestcontainersKotlinApplication>(*args)
}
